package com.planieasy.lessonplanning.service;

import com.planieasy.lessonplanning.dto.*;
import com.planieasy.lessonplanning.dto.indicators.IndicatorOneContentUpdated;
import com.planieasy.lessonplanning.dto.indicators.IndicatorThreeContentUpdated;
import com.planieasy.lessonplanning.dto.indicators.IndicatorTwoContentUpdated;
import com.planieasy.lessonplanning.dto.skills.SkillOneDescriptionUpdated;
import com.planieasy.lessonplanning.dto.skills.SkillThreeDescriptionUpdated;
import com.planieasy.lessonplanning.dto.skills.SkillTwoDescriptionUpdated;
import com.planieasy.lessonplanning.model.LessonPlanning;
import com.planieasy.lessonplanning.repository.LessonPlanningDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LessonPlanningService {
    @Autowired
    LessonPlanningDAO lessonPlanningDAO;

    @Autowired
    IAssistantService iAssistantService;



    public LessonPlanning redoLessonPlanning(Integer idLessonPlanning){
        LessonPlanning oldLessonPlanning=lessonPlanningDAO.findById(idLessonPlanning).get();
        String topicLessonPlanning=oldLessonPlanning.getLesPlaTopic();

        PlanningSections planningSections=iAssistantService.redoLessonPlanning(oldLessonPlanning.getLesPlaOAId(), topicLessonPlanning);

        oldLessonPlanning.setLesPlaTopic(planningSections.titulo);
        oldLessonPlanning.setLesPlaBeginning(planningSections.inicio);
        oldLessonPlanning.setLesPlaDevelopment(planningSections.desarrollo);
        oldLessonPlanning.setLesPlaClosing(planningSections.cierre);
        oldLessonPlanning.setLesPlaSkillOne(planningSections.habilidades[0]);
        oldLessonPlanning.setLesPlaSkillTwo(planningSections.habilidades[1]);
        oldLessonPlanning.setLesPlaSkillThree(planningSections.habilidades[2]);
        oldLessonPlanning.setLesPlaIndOne(planningSections.indicadores[0]);
        oldLessonPlanning.setLesPlaIndTwo(planningSections.indicadores[1]);
        oldLessonPlanning.setLesPlaIndThree(planningSections.indicadores[2]);
        return lessonPlanningDAO.save(oldLessonPlanning);
    }



    public LessonPlanning addLessonPlanning(LessonPlanning newLessonPlanning){
        PlanningSections planningSections=iAssistantService.getFullPlanification(newLessonPlanning.getLesPlaOAId());
        newLessonPlanning.setLesPlaTopic(planningSections.titulo);
        newLessonPlanning.setLesPlaBeginning(planningSections.inicio);
        newLessonPlanning.setLesPlaDevelopment(planningSections.desarrollo);
        newLessonPlanning.setLesPlaClosing(planningSections.cierre);
        //Acá hay que crear los indicadores y las habilidades correspondientes

        newLessonPlanning.setLesPlaIndOne(planningSections.indicadores[0]);
        newLessonPlanning.setLesPlaIndTwo(planningSections.indicadores[1]);
        newLessonPlanning.setLesPlaIndThree(planningSections.indicadores[2]);

        newLessonPlanning.setLesPlaSkillOne(planningSections.habilidades[0]);
        newLessonPlanning.setLesPlaSkillTwo(planningSections.habilidades[1]);
        newLessonPlanning.setLesPlaSkillThree(planningSections.habilidades[2]);

        return lessonPlanningDAO.save(newLessonPlanning);
    }



    public List<LessonPlanning> getLessonPlanningsBySubject(Integer subId){
        return lessonPlanningDAO.findBySubId(subId);
    }

    public Optional<LessonPlanning> getLessonPlanningById(Integer lessPlaId){
        return lessonPlanningDAO.findById(lessPlaId);
    }

    public LessonPlanning updateLesPlaDate(DateLessonUpdated dateLessonUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(dateLessonUpdated.getIdLessonPlanning()).get();
        lessonPlanning.setLesPlaDate(dateLessonUpdated.getNewDate());
        return lessonPlanningDAO.save(lessonPlanning);
    }

    public LessonPlanning updateLesPlaBeginning(BeginningUpdated beginningUpdated){

        LessonPlanning lessonPlanning = getLessonPlanningById(beginningUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaBeginning(beginningUpdated.getNewBeginning());
        return lessonPlanningDAO.save(lessonPlanning);
    }

    public LessonPlanning updateLesPlaDevelopment(DevelopmentUpdated developmentUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(developmentUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaDevelopment(developmentUpdated.getNewDevelopment());
        return lessonPlanningDAO.save(lessonPlanning);
    }

    public LessonPlanning updateLesPlaClosing(ClosingUpdated closingUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(closingUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaClosing(closingUpdated.getNewClosing());
        return lessonPlanningDAO.save(lessonPlanning);
    }


    //Indicadores
    public LessonPlanning updateLesPlaIndOne(IndicatorOneContentUpdated indicatorOneContentUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(indicatorOneContentUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaIndOne(indicatorOneContentUpdated.getNewIndicatorOneDescription());
        return lessonPlanningDAO.save(lessonPlanning);
    }
    public LessonPlanning updateLesPlaIndTwo(IndicatorTwoContentUpdated indicatorTwoContentUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(indicatorTwoContentUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaIndTwo(indicatorTwoContentUpdated.getNewIndicatorTwoDescription());
        return lessonPlanningDAO.save(lessonPlanning);
    }
    public LessonPlanning updateLesPlaIndThree(IndicatorThreeContentUpdated indicatorThreeContentUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(indicatorThreeContentUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaIndThree(indicatorThreeContentUpdated.getNewIndicatorThreeDescription());
        return lessonPlanningDAO.save(lessonPlanning);
    }

    //Habilidades
    public LessonPlanning updateLesPlaSkillOne(SkillOneDescriptionUpdated skillOneDescriptionUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(skillOneDescriptionUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaSkillOne(skillOneDescriptionUpdated.getNewSkillOneDescription());
        return lessonPlanningDAO.save(lessonPlanning);
    }
    public LessonPlanning updateLesPlaSkillTwo(SkillTwoDescriptionUpdated skillTwoDescriptionUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(skillTwoDescriptionUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaSkillTwo(skillTwoDescriptionUpdated.getNewSkillTwoDescription());
        return lessonPlanningDAO.save(lessonPlanning);
    }
    public LessonPlanning updateLesPlaSkillThree(SkillThreeDescriptionUpdated skillThreeDescriptionUpdated){
        LessonPlanning lessonPlanning = getLessonPlanningById(skillThreeDescriptionUpdated.getLesPlaId()).get();
        lessonPlanning.setLesPlaSkillThree(skillThreeDescriptionUpdated.getNewSkillThreeDescription());
        return lessonPlanningDAO.save(lessonPlanning);
    }
}
