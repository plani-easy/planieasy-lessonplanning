package com.planieasy.lessonplanning.service;


import com.planieasy.lessonplanning.dto.PlanningSections;
import io.github.flashvayne.chatgpt.service.ChatgptService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RequiredArgsConstructor
@Service
public class IAssistantService {
    @Autowired
    private final ChatgptService chatgptService;

    @Autowired
    private RestTemplate restTemplate;


    public PlanningSections getFullPlanification(Integer learningObjectiveId){ //Devuelve un DTO con cada sección de la Plani para ser consumido por addLessonPlanning en LessonPlanningService
        String url = "https://plani-easy-backend.onrender.com/oa/" + learningObjectiveId;
        KnowledgeObjective knowledgeObjective = restTemplate.getForObject(url, KnowledgeObjective.class);
        String objDescription = knowledgeObjective.getObjDescription();

        String requestSetting="Cada sección de la planificación estará orientada por un ‘Objetivo de Aprendizaje’. La planificación tendrá las siguientes secciones: Título de la clase, Habilidades, Indicadores de evaluación, Inicio, Desarrollo y Cierre. Realizar la clase en torno a un solo Tópico en específico relacionado con el Objetivo de Aprendizaje que satisfaga las actividades que plantea este último, como definir, identificar, reflexionar, etc. No obstante, se debe orientar la clase en torno a una obra artística o literaria que se deba analizar para satisfacer el objetivo de aprendizaje, puede ser una película, un libro, una serie, etc. Esta obra debe estar en el título de la clase. Título de la clase: Debe ser una frase corta pero asertiva y precisa, además de contener el nombre de la obra a analizar en la clase en su composición. Habilidades: Corresponden a las habilidades específicas que los alumnos desarrollarán durante la clase, en base a las actividades realizadas y los Objetivos de Aprendizaje. Indicadores de evaluación: Corresponden a los conocimientos y habilidades que se espera que los estudiantes adquieran como resultado de la clase, relacionado con el Objetivo de Aprendizaje. Inicio: Un inicio de clase es el punto de partida de una sesión educativa, estableciendo el tono y presentando el enfoque para la clase. Desarrollo: Durante el desarrollo de la clase, se promueve la participación activa de los estudiantes a través de diversas dinámicas, discusiones y actividades, todas diseñadas para explorar en profundidad el tópico específico de esa clase y alcanzar total o parcialmente el objetivo de aprendizaje. Cierre: El cierre de clase implica una reflexión grupal o plenaria adaptada al tópico específico de cualquier asignatura seleccionado, conciso pero descriptivo, resaltando los aspectos clave discutidos durante la clase y su relevancia para futuras reflexiones o acciones. Título de la clase: Debe ser una frase corta pero precisa. Habilidades: Generar 3. Comenzando cada una con un verbo relacionado a los Objetivos de Aprendizaje. Indicadores de evaluación: Siempre deben arrancar con la frase ‘Los estudiantes…’, reflejando los conocimientos y habilidades específicos que se espera que los estudiantes adquieran como resultado de la clase. Inicio: Debe ser descrito en tercera persona, adaptado al tópico específico de cualquier asignatura seleccionado, conciso pero descriptivo. Desarrollo: Debe ser descrito en tercera persona, adaptado al tópico específico de la clase, conciso, descriptivo y detallado para explorar en profundidad el tema. Debe poseer una actividad principal. Cierre: Debe ser descrito en tercera persona, conciso pero descriptivo, resaltando los aspectos clave discutidos durante la clase y su relevancia para futuras reflexiones. \n" +
                "El formato de respuesta será un '#' para marcar las secciones y un '_' para cada habilidad e indicador, de forma '_Habilidad N' o '*Indicador N':\n" +
                "\n" +
                "#Título:[descripcionTitulo]\n" +
                "#Habilidades:\n" +
                "*Habilidad 1: [descripcionHabilidad1] \n" +
                "*Habilidad 2: [descripcionHabilidad2]\n" +
                "*Habilidad 3: [descripcionHabilidad3]\n" +
                "#Indicadores:\n" +
                "_Indicador 1: [descripcionIndicador1]\n" +
                "_Indicador 2: [descripcionIndicador2]\n" +
                "_Indicador 3: [descripcionIndicador3]\n" +
                "#Inicio: [descripcionInicio]\n" +
                "#Desarrollo: [descripcionDesarrollo]\n" +
                "#Cierre: [descripcionCierre].\n" +
                "\n" +
                "Considerando lo anterior genera una planificación para el siguiente Objetivo de Aprendizaje: "+objDescription+" Recuerda seguir el formato exacto y no poner texto demás, ya que la respuesta será deserializada a partir del formato. ";
        return deserializarPlanificacion(chatgptService.sendMessage(requestSetting));
    }

    public static PlanningSections deserializarPlanificacion(String planificacion) {
        Pattern pattern = Pattern.compile("#Título: (.*?)#Habilidades:(.*?)#Indicadores:(.*?)#Inicio:(.*?)#Desarrollo:(.*?)#Cierre:(.*?)$", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(planificacion);
        if (matcher.find()) {
            PlanningSections plan = new PlanningSections();
            plan.titulo = matcher.group(1).trim();
            plan.habilidades = limpiarLista(matcher.group(2));
            plan.indicadores = limpiarLista(matcher.group(3));
            plan.inicio = matcher.group(4).trim();
            plan.desarrollo = matcher.group(5).trim();
            plan.cierre = matcher.group(6).trim();
            return plan;
        }
        return null;
    }

    private static String[] limpiarLista(String lista) {
        return lista.lines()
                .map(String::trim)
                .filter(line -> !line.isEmpty()) // Filtrar líneas vacías
                .map(line -> line.replaceFirst("[*_]", "")) // Eliminar '*' o '_' al inicio de cada línea
                .toArray(String[]::new);
    }

    public PlanningSections redoLessonPlanning(Integer lesPlaOAId, String topicLessonPlanning) {
        String url = "http://localhost:8089/oa/" + lesPlaOAId;
        KnowledgeObjective knowledgeObjective = restTemplate.getForObject(url, KnowledgeObjective.class);
        String objDescription = knowledgeObjective.getObjDescription();

        String requestSetting="Cada sección de la planificación estará orientada por un ‘Objetivo de Aprendizaje’. La planificación tendrá las siguientes secciones: Título de la clase, Habilidades, Indicadores de evaluación, Inicio, Desarrollo y Cierre. Realizar la clase en torno a un solo Tópico en específico relacionado con el Objetivo de Aprendizaje que satisfaga las actividades que plantea este último, como definir, identificar, reflexionar, etc. No obstante, se debe orientar la clase en torno a una obra artística o literaria que se deba analizar para satisfacer el objetivo de aprendizaje, puede ser una película, un libro, una serie, etc. Esta obra debe estar en el título de la clase. Título de la clase: Debe ser una frase corta pero asertiva y precisa, además de contener el nombre de la obra a analizar en la clase en su composición. Habilidades: Corresponden a las habilidades específicas que los alumnos desarrollarán durante la clase, en base a las actividades realizadas y los Objetivos de Aprendizaje. Indicadores de evaluación: Corresponden a los conocimientos y habilidades que se espera que los estudiantes adquieran como resultado de la clase, relacionado con el Objetivo de Aprendizaje. Inicio: Un inicio de clase es el punto de partida de una sesión educativa, estableciendo el tono y presentando el enfoque para la clase. Desarrollo: Durante el desarrollo de la clase, se promueve la participación activa de los estudiantes a través de diversas dinámicas, discusiones y actividades, todas diseñadas para explorar en profundidad el tópico específico de esa clase y alcanzar total o parcialmente el objetivo de aprendizaje. Cierre: El cierre de clase implica una reflexión grupal o plenaria adaptada al tópico específico de cualquier asignatura seleccionado, conciso pero descriptivo, resaltando los aspectos clave discutidos durante la clase y su relevancia para futuras reflexiones o acciones. Título de la clase: Debe ser una frase corta pero precisa. Habilidades: Generar 3. Comenzando cada una con un verbo relacionado a los Objetivos de Aprendizaje. Indicadores de evaluación: Siempre deben arrancar con la frase ‘Los estudiantes…’, reflejando los conocimientos y habilidades específicos que se espera que los estudiantes adquieran como resultado de la clase. Inicio: Debe ser descrito en tercera persona, adaptado al tópico específico de cualquier asignatura seleccionado, conciso pero descriptivo. Desarrollo: Debe ser descrito en tercera persona, adaptado al tópico específico de la clase, conciso, descriptivo y detallado para explorar en profundidad el tema. Debe poseer una actividad principal. Cierre: Debe ser descrito en tercera persona, conciso pero descriptivo, resaltando los aspectos clave discutidos durante la clase y su relevancia para futuras reflexiones. \n" +
                "El formato de respuesta será un '#' para marcar las secciones y un '_' para cada habilidad e indicador, de forma '_Habilidad N' o '*Indicador N':\n" +
                "\n" +
                "#Título:[descripcionTitulo]\n" +
                "#Habilidades:\n" +
                "*Habilidad 1: [descripcionHabilidad1] \n" +
                "*Habilidad 2: [descripcionHabilidad2]\n" +
                "*Habilidad 3: [descripcionHabilidad3]\n" +
                "#Indicadores:\n" +
                "_Indicador 1: [descripcionIndicador1]\n" +
                "_Indicador 2: [descripcionIndicador2]\n" +
                "_Indicador 3: [descripcionIndicador3]\n" +
                "#Inicio: [descripcionInicio]\n" +
                "#Desarrollo: [descripcionDesarrollo]\n" +
                "#Cierre: [descripcionCierre].\n" +
                "\n" +
                "Considerando lo anterior genera una planificación para el siguiente Objetivo de Aprendizaje: "+objDescription+", cuya clase se base en cualquier temática que no sea "+topicLessonPlanning+" Recuerda seguir el formato exacto y no poner texto demás, ya que la respuesta será deserializada a partir del formato. También recuerda incluir actividades entretenidas, ingeniosas y diversas para todas las clases sean únicas";
        return deserializarPlanificacion(chatgptService.sendMessage(requestSetting));

    }

    @Getter
    private static class KnowledgeObjective {
        private Integer objId;
        private Integer objSubCode;
        private Integer objGradeCodeLevel;
        private Integer objNumber;
        private String objDescription;
    }
}
