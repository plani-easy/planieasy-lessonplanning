package com.planieasy.lessonplanning.dto;

import lombok.Data;

@Data
public class BeginningUpdated {
    private String newBeginning;
    private Integer lesPlaId;
}
