package com.planieasy.lessonplanning.dto;

import lombok.Data;

@Data
public class PlanningSections {
    public String titulo;
    public String[] habilidades;
    public String[] indicadores;
    public String inicio;
    public String desarrollo;
    public String cierre;
}
