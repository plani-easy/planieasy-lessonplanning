package com.planieasy.lessonplanning.dto.indicators;

import lombok.Data;

@Data
public class IndicatorTwoContentUpdated {
    private String newIndicatorTwoDescription;
    private Integer lesPlaId;
}
