package com.planieasy.lessonplanning.dto.indicators;

import lombok.Data;

@Data
public class IndicatorOneContentUpdated {
    private String newIndicatorOneDescription;
    private Integer lesPlaId;
}
