package com.planieasy.lessonplanning.dto.indicators;

import lombok.Data;

@Data
public class IndicatorThreeContentUpdated {
    private String newIndicatorThreeDescription;
    private Integer lesPlaId;
}