package com.planieasy.lessonplanning.dto;

import lombok.Data;

@Data
public class DateLessonUpdated {
    private String newDate;
    private Integer idLessonPlanning;
}
