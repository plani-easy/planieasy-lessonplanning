package com.planieasy.lessonplanning.dto;

import lombok.Data;

@Data
public class ClosingUpdated {
    private String newClosing;
    private Integer lesPlaId;
}
