package com.planieasy.lessonplanning.dto;

import lombok.Data;

@Data
public class DevelopmentUpdated {
    private String newDevelopment;
    private Integer lesPlaId;
}
