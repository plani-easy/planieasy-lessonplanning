package com.planieasy.lessonplanning.dto.skills;

import lombok.Data;

@Data
public class SkillThreeDescriptionUpdated {
    private String newSkillThreeDescription;
    private Integer lesPlaId;
}
