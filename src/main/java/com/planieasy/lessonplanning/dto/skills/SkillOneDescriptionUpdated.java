package com.planieasy.lessonplanning.dto.skills;

import lombok.Data;

@Data
public class SkillOneDescriptionUpdated {
    private String newSkillOneDescription;
    private Integer lesPlaId;
}
