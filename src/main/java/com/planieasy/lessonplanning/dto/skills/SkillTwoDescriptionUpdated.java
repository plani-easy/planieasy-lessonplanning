package com.planieasy.lessonplanning.dto.skills;

import lombok.Data;

@Data
public class SkillTwoDescriptionUpdated {
    private String newSkillTwoDescription;
    private Integer lesPlaId;
}
