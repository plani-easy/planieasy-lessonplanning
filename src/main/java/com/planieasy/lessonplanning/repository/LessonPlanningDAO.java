package com.planieasy.lessonplanning.repository;

import com.planieasy.lessonplanning.model.LessonPlanning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LessonPlanningDAO extends JpaRepository<LessonPlanning, Integer> {
    @Query("SELECT a FROM LessonPlanning a WHERE a.subId = :subId")
    List<LessonPlanning> findBySubId(@Param("subId") Integer subId);
}
