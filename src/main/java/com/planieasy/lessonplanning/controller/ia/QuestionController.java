package com.planieasy.lessonplanning.controller.ia;

import com.planieasy.lessonplanning.dto.PlanningSections;
import com.planieasy.lessonplanning.service.IAssistantService;
import io.github.flashvayne.chatgpt.service.ChatgptService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "https://planieasy-frontend.onrender.com", methods={RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping("/questions")
public class QuestionController {
    @Autowired
    private final ChatgptService chatgptService;

    @Autowired
    private IAssistantService iAssistantService;


    @GetMapping("/send")
    public String send(@RequestBody String message){
        String responseMessage = chatgptService.sendMessage(message);
        return responseMessage;
    }

    //este servirá pa probar el método de IAssistant
    @GetMapping("/iassistant/{idOa}")
    public PlanningSections send(@PathVariable Integer idOa){
        return iAssistantService.getFullPlanification(idOa);
    }
}
