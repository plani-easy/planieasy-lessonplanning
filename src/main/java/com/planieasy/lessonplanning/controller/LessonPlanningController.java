package com.planieasy.lessonplanning.controller;

import com.planieasy.lessonplanning.dto.BeginningUpdated;
import com.planieasy.lessonplanning.dto.ClosingUpdated;
import com.planieasy.lessonplanning.dto.DateLessonUpdated;
import com.planieasy.lessonplanning.dto.DevelopmentUpdated;
import com.planieasy.lessonplanning.dto.indicators.IndicatorOneContentUpdated;
import com.planieasy.lessonplanning.dto.indicators.IndicatorThreeContentUpdated;
import com.planieasy.lessonplanning.dto.indicators.IndicatorTwoContentUpdated;
import com.planieasy.lessonplanning.dto.skills.SkillOneDescriptionUpdated;
import com.planieasy.lessonplanning.dto.skills.SkillThreeDescriptionUpdated;
import com.planieasy.lessonplanning.dto.skills.SkillTwoDescriptionUpdated;
import com.planieasy.lessonplanning.model.LessonPlanning;
import com.planieasy.lessonplanning.service.LessonPlanningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "https://planieasy-frontend.onrender.com", methods={RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping("/lessons")
public class LessonPlanningController {
    @Autowired
    LessonPlanningService lessonPlanningService;

    @PostMapping("/add")
    public LessonPlanning addLessonPlanning(@RequestBody LessonPlanning newLessonPlanning){
        return lessonPlanningService.addLessonPlanning(newLessonPlanning);
    }

    @GetMapping("/bySubject/{subjectId}")
    public List<LessonPlanning> getLessonsBySubject(@PathVariable Integer subjectId){
        return lessonPlanningService.getLessonPlanningsBySubject(subjectId);
    }

    @GetMapping("/{idLesson}")
    public Optional<LessonPlanning> getLessonById(@PathVariable Integer idLesson){
        return lessonPlanningService.getLessonPlanningById(idLesson);
    }

    @PostMapping("/updateDate")
    public LessonPlanning updateContentDescription(@RequestBody DateLessonUpdated dateLessonUpdated){
        return lessonPlanningService.updateLesPlaDate(dateLessonUpdated);
    }

    @PostMapping("/updateBeginning")
    public LessonPlanning updateBeginning(@RequestBody BeginningUpdated beginningUpdated){
        return lessonPlanningService.updateLesPlaBeginning(beginningUpdated);
    }

    @PostMapping("/updateDevelopment")
    public LessonPlanning updateDevelopment(@RequestBody DevelopmentUpdated developmentUpdated){
        return lessonPlanningService.updateLesPlaDevelopment(developmentUpdated);
    }

    @PostMapping("/updateClosing")
    public LessonPlanning updateClosing(@RequestBody ClosingUpdated closingUpdated){
        return lessonPlanningService.updateLesPlaClosing(closingUpdated);
    }

    //Actualizar Indicadores
    @PostMapping("/updateIndicatorOne")
    public LessonPlanning updateIndicatorOne(@RequestBody IndicatorOneContentUpdated indicatorOneContentUpdated){
        return lessonPlanningService.updateLesPlaIndOne(indicatorOneContentUpdated);
    }
    @PostMapping("/updateIndicatorTwo")
    public LessonPlanning updateIndicatorTwo(@RequestBody IndicatorTwoContentUpdated indicatorTwoContentUpdated){
        return lessonPlanningService.updateLesPlaIndTwo(indicatorTwoContentUpdated);
    }
    @PostMapping("/updateIndicatorThree")
    public LessonPlanning updateIndicatorThree(@RequestBody IndicatorThreeContentUpdated indicatorThreeContentUpdated){
        return lessonPlanningService.updateLesPlaIndThree(indicatorThreeContentUpdated);
    }


    //Actualizar Habilidades
    @PostMapping("/updateSkillOne")
    public LessonPlanning updateSkillOne(@RequestBody SkillOneDescriptionUpdated skillOneDescriptionUpdated){
        return lessonPlanningService.updateLesPlaSkillOne(skillOneDescriptionUpdated);
    }
    @PostMapping("/updateSkillTwo")
    public LessonPlanning updateSkillTwo(@RequestBody SkillTwoDescriptionUpdated skillTwoDescriptionUpdated){
        return lessonPlanningService.updateLesPlaSkillTwo(skillTwoDescriptionUpdated);
    }
    @PostMapping("/updateSkillThree")
    public LessonPlanning updateSkillThree(@RequestBody SkillThreeDescriptionUpdated skillThreeDescriptionUpdated){
        return lessonPlanningService.updateLesPlaSkillThree(skillThreeDescriptionUpdated);
    }

    @PutMapping("/redo/{idLessonPlanning}")
    public LessonPlanning redoLessonPlanning(@PathVariable Integer idLessonPlanning){
        return lessonPlanningService.redoLessonPlanning(idLessonPlanning);
    }
}