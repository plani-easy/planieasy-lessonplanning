package com.planieasy.lessonplanning.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "LessonPlanning")
public class LessonPlanning {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer lesPlaid;

    @Column(name = "LesPlaTopic")
    private String lesPlaTopic;

    /*
    @ManyToOne
    @JoinColumn(name = "subId", nullable = true)
    private Subject subId;
     */


    @Column(name = "subId", nullable = true)
    private Integer subId;
    @Column(name = "lesPlaOAId")
    private Integer lesPlaOAId;
    @Column(name = "lesPlaDate")
    private String lesPlaDate;
    @Column(name = "lesPlaBeginning", length = 1024)
    private String lesPlaBeginning;
    @Column(name = "lesPlaDevelopment", length = 1024)
    private String lesPlaDevelopment;
    @Column(name = "lesPlaClosing", length = 1024)
    private String lesPlaClosing;
    //Indicadores
    @Column(name = "lesPlaIndOne", length = 1024)
    private String lesPlaIndOne;
    @Column(name = "lesPlaIndTwo", length = 1024)
    private String lesPlaIndTwo;
    @Column(name = "lesPlaIndThree", length = 1024)
    private String lesPlaIndThree;
    //Habilidades
    @Column(name = "lesPlaSkillOne", length = 1024)
    private String lesPlaSkillOne;
    @Column(name = "lesPlaSkillTwo", length = 1024)
    private String lesPlaSkillTwo;
    @Column(name = "lesPlaSkillThree", length = 1024)
    private String lesPlaSkillThree;
}