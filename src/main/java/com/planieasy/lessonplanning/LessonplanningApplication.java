package com.planieasy.lessonplanning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LessonplanningApplication {

	public static void main(String[] args) {
		SpringApplication.run(LessonplanningApplication.class, args);
	}

}
