FROM openjdk:17-oracle
MAINTAINER AVY
COPY target/lessonplanning-0.0.1-SNAPSHOT.jar  api-lessonplanning-app.jar
ENTRYPOINT ["java","-jar","/api-lessonplanning-app.jar"]